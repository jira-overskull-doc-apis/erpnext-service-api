<?php
/*
 * (c) OVERSKULL <overskull@overskull.pe>
 * This source file is subject to the MIT license that is bundled
 * Credit PLoco new
 *
 */

namespace overskull\ErpNext;

class Erpnext
{
    public $pathLogin;

    function set_config($pathLogin, $user, $pass)
    {
        $this->pathLogin = $pathLogin;
        $this->user = $user;
        $this->pass = $pass;
    }

    function apiLogin($gunzzle)
    {

        $client = $gunzzle;
        $response = $client->request('GET', $this->pathLogin, [
            'form_params' => [
                'usr' => $this->user,
                'pwd' => $this->pass
            ]
        ]);
        $cookie = $response->getHeaders()['Set-Cookie'];
        $fichero = base_path() . '/public/cookies/cookieserpoverskull.json';
        file_put_contents($fichero, $cookie);

        return $fichero;
    }

    function apiService($urlService, $urlOptions, $metodo, $data)
    {
        $path = $urlService;
        $pathApi = $path . $urlOptions;
        $fichero = base_path() . '/public/cookies/cookieserpoverskull.json';
        $cookie = file_get_contents($fichero);
        $data_json = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $pathApi);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie:' . $cookie, 'Accept: application/json', 'Content-Type: aplication/json'));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $metodo);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            $mensaje = "cURL Error #:" . $err;
        } else {
            $mensaje = $response;
        }
        return $mensaje;
    }
}